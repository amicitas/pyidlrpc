#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ==============================================================================
"""
author
======

Novimir Antoniuk Pablant
- npablant@pppl.gov
- novimir.pablant@amicitas.com


purpose
=======
Build the pyidlrpc module.


description
===========

This is a standard python setup script that can be used to build and
install the pyidlrpc module.  This suports all of the standard distuils 
commands. If setuputils is installed, the extended commands will work as 
well.

To install pyidlrpc use the following command:
python setup.py install

To build the pyidlrpc module as a library (*.so) that can be directly
imported from python use:
python setup.py build_ext --inplace


This setup script also includes a custom clean command which will remove
any build files from the source directory.  The optional keword all will
also remove the built library.  For example:
python setup.py clean --all


requirements
============

Python 2.7, 3.0 or later
Cython

Python Packages:
  numpy
  psutil 2.0 or later

All of these dependencies are available on all platforms using your
favorite package manager (macports, easy_install, apt-get, etc.)
   

"""

import os
import platform
import subprocess
import numpy

version = '0.1.6'

try:
    from setuptools.core import setup
except:
    from distutils.core import setup

from distutils.cmd import Command
from distutils.extension import Extension

# Since pyidlrpc is still in the development stage I am going
# to always use cython if it is available.
use_cython = True
try:
    from Cython.Build import cythonize
except ImportError:
    use_cython = False

if platform.architecture()[0] == '32bit':
    machine = 'i386'
elif platform.architecture()[0] == '64bit':
    machine = 'x86_64'
else:
    raise Exception("Platform type could not be determined.")



# Find the idl installation directory. And setup shared libraries.
#
# The installaiton directory can almost always be found using the 'IDL_DIR' 
# environment variable. The problem though is that installation will sometimes 
# require the use of sudo, which on some systems does not preserve environment 
# variables.
if platform.system() == 'Darwin':
    if 'IDL_DIR' in os.environ:
        idl_dir = os.environ['IDL_DIR']
    elif os.path.exists('/Applications/itt/idl/idl/bin/'):
        idl_dir = '/Applications/itt/idl/idl/'
    elif os.path.exists('/Applications/exelis/idl/bin/'):
        idl_dir = '/Applications/exelis/idl/'
    else:
        raise Exception("Could not find idl installation directory.  Use the --idl-dir option.")
                        
    idl_bin_dir = idl_dir+'/bin/bin.darwin.'+machine

    extra_link_args = ['-Wl,-rpath,'+idl_bin_dir]

        
    # Find the name of the libidl_rpc.dylib for the current IDL version.
    libidl_rpc = 'libidl_rpc.dylib'
    libidl_rpc_path = os.path.join(idl_bin_dir, libidl_rpc)

    libidl_rpc_path = os.path.realpath(libidl_rpc_path)
    libidl_rpc = os.path.basename(libidl_rpc_path)

    # On OS X, the linker will only find libaries in the rpath directory if
    # they have an installation name starting with @rpath.  Since this is not
    # the default for libidl_rpc, we change the name here.
    #
    # Hopefully this wont affect anything else on the users system.
    otool_output = subprocess.check_output(['otool', '-D', libidl_rpc_path])
    libidl_rpc_name_orig = otool_output.splitlines()[1].decode()
    if libidl_rpc_name_orig.find('@rpath') != 0:
        # The idl_rpc library does not have the correct name. Rename it now.
        print('\nThe installation name of libidl_rpc.dylib must be changed to\n' 
              '@rpath/libidl_rpc.dylib for pyidlrpc to properly link to the library.\n'
              'Renaming requires root privalages. (Press control-c to abort.)\n')
        try:
            subprocess.check_call(['sudo', 'install_name_tool', '-id', '@rpath/'+libidl_rpc, libidl_rpc_path])

            otool_output = subprocess.check_output(['otool', '-D', libidl_rpc_path])
            libidl_rpc_name_new = otool_output.splitlines()[1].decode()
            if libidl_rpc_name_new.find('@rpath') != 0:
                raise Exception
            print('Renamed the idl_rpc shared library from {} to {}.'.format(libidl_rpc_name_orig, libidl_rpc_name_new))

        except:
            print('\nUnable to change the installation name of the libidl_rpc.dylib library.\n'
                  'This can be done manually by using the command:\n'
                  '   sudo install_name_tool -id @rpath/'+libidl_rpc+' '+libidl_rpc_path+'\n'
                  'Alternively add the following line to your .bashrc file:\n'
                  '   export DYLD_FALLBACK_LIBRARY_PATH=$DYLD_FALLBACK_LIBRARY_PATH:$IDL_DIR/bin/bin.darwin.'+machine+'\n'
                  '\n')

    
elif platform.system() == 'Linux':
    if 'IDL_DIR' in os.environ:
        idl_dir = os.environ['IDL_DIR']
    elif os.path.exists('/usr/local/itt/idl/idl/bin/'):
        idl_dir = '/usr/local/itt/idl/idl/'
    elif os.path.exists('/usr/local/exelis/idl/bin/'):
        idl_dir = '/usr/local/exelis/idl/'
    else:
        raise Exception("Could not find idl installation directory.  Use the --idl-dir option.")
    
    idl_bin_dir = idl_dir+'/bin/bin.linux.'+machine

    extra_link_args = []
        
else:
    raise Exception("Location of IDL bin directory unknown for platform: {}.".format(platform.system()))



# Choose which libraries to use.  I was trying to build a version
# of idlrpc that could handle structures.  For the time being I am
# giving up on this though and just using the version shiped with
# IDL.
use_modified_idlrpc = False

if use_modified_idlrpc:
    include_dirs = ['/u/npablant/code/utilities/python/mir_idlrpc/rpc'
                    ,numpy.get_include()]
    library_dirs = ['/u/npablant/code/utilities/python/mir_idlrpc/rpc'
                    ,idl_bin_dir]
    libraries = ['idl_rpc', 'idl']
else:
    include_dirs = [idl_dir+'/external/include'
                    ,numpy.get_include()]
    library_dirs = [idl_bin_dir]
    libraries = ['idl_rpc']


# Define the extension.
#
# There is a bug in cython that does not allow me to create
# certain numpy objects if language="c++".  With c I get
# warnings instead of errors. 
# (I don't know if this is still true 2014-01-06)

if use_cython:
    ext = '.pyx'
else:
    ext = '.c'
    
extensions = [
    Extension('pyidlrpc'
              ,['pyidlrpc'+ext]
              ,include_dirs=include_dirs
              ,libraries=libraries
              ,library_dirs=library_dirs
              ,runtime_library_dirs=[idl_bin_dir]
              ,extra_link_args=extra_link_args
              )
    ]
        
# Cythonize.
if use_cython:
    extensions = cythonize(extensions)


class CleanCommand(Command):
    description = "custom clean command that forcefully removes dist/build directories"
    user_options = [("all", None, "Clean all files.")]
    def initialize_options(self):
        self.cwd = None
        self.all = None
    def finalize_options(self):
        self.cwd = os.getcwd()
    def run(self):
        assert os.getcwd() == self.cwd, 'Must be in package root: %s' % self.cwd
        os.system('rm -rf ./build ./pyidlrpc.c ./pyidlrpc.cpp ./setup.cfg')
        if self.all:
            os.system('rm -rf ./pyidlrpc.so ./pyidlrpc.egg-info ./dist')


# Read in the README file to use as a long description.
with open('README') as file:
    long_description = file.read()

# Options for PyPI
classifiers=[
    "Development Status :: 4 - Beta"
    ,"License :: OSI Approved :: MIT License"
    ,"Topic :: Software Development :: Interpreters"
    ,"Programming Language :: Cython"
    ,"Programming Language :: Python"
    ,"Programming Language :: Other"
    ]

# Setup options for setup.
params = {'name':'pyidlrpc'
          ,'version':version
          ,'description':'A library to call IDL (Interactive Data Language) from python.'
          ,'long_description':long_description
          ,'author':'Novimir Antoniuk Pablant'
          ,'author_email':'novimir.pablant@amicitas.com'
          ,'url':'http://amicitas.bitbucket.org/pyidlrpc/'
          ,'license':'MIT'
          ,'ext_modules':extensions
          ,'packages':None
          ,'classifiers':classifiers
          ,'install_requires':['numpy', 'psutil>=2.0']
          }

# Override the C-extension building so that it knows about '.pyx'
# Cython files
params['cmdclass'] = dict(clean=CleanCommand)

# Call the actual building/packaging function (see distutils docs)
setup(**params)
