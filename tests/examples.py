

def test_example_01():

    print('\n'+'-'*80)
    print('Starting test_example_01')
    
    # Import pyidlrpc.
    import pyidlrpc as idl

    # Execute a command in IDL.
    # This will print 'Hello mom!' in the idlrpc window.
    idl.execute("PRINT, 'Hello mom!'")
    
    print('test_example_01 sucessful.')  

    
def test_example_02():
    
    print('\n'+'-'*80)
    print('Starting test_example_02')
    
    import numpy as np
    import pyidlrpc as idl

    # Create a numpy array in python.
    py_array = np.random.normal(size=1000)

    # Copy this data to IDL.
    idl.setVariable('idl_array', py_array)

    # Calculate the standard devation and the mean in IDL.
    idl.execute('idl_stddev = STDDEV(idl_array)')
    idl.execute('idl_mean = MEAN(idl_array)')

    # Copy the results back to python.
    py_stddev = idl.getVariable('idl_stddev')
    py_mean = idl.getVariable('idl_mean')

    # Print out the results.
    print('Mean: {}, StdDev: {}'.format(py_mean, py_stddev))

    assert py_mean > -0.1 and py_mean < 0.1, 'py_mean is out of range.'
    assert py_stddev > 0.9 and py_stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_02 sucessful.')

    
def test_example_03():
    
    print('\n'+'-'*80)
    print('Starting test_example_03')
    
    import numpy as np
    import pyidlrpc as idl

    # Create a numpy array in python.
    py_array = np.random.normal(size=1000)

    # Calculate the standard devication and mean using IDL.
    py_stddev = idl.callFunction('STDDEV', [py_array])
    py_mean = idl.callFunction('MEAN', [py_array])

    # Print out the results.
    print('Mean: {}, StdDev: {}'.format(py_mean, py_stddev))
    
    assert py_mean > -0.1 and py_mean < 0.1, 'py_mean is out of range.'
    assert py_stddev > 0.9 and py_stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_03 sucessful.')    

    
def test_example_04():
    
    print('\n'+'-'*80)
    print('Starting test_example_04')
    
    import numpy as np
    from pyidlrpc import PyIDL

    idl = PyIDL()

    # Create a numpy array in python.
    py_array = np.random.normal(size=1000)

    # Calculate the standard devication and mean using IDL.
    py_stddev = idl.callFunction('STDDEV', [py_array])
    py_mean = idl.callFunction('MEAN', [py_array])
    
    assert py_mean > -0.1 and py_mean < 0.1, 'py_mean is out of range.'
    assert py_stddev > 0.9 and py_stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_04 sucessful.')  

    
def test_example_05():
    
    print('\n'+'-'*80)
    print('Starting test_example_05')

    import numpy as np
    import example_05_idlmath

    array = np.random.normal(size=1000)

    # Here we transparently call the wrapped IDL functions.
    mean = example_05_idlmath.mean(array)
    stddev = example_05_idlmath.stddev(array)
    
    assert mean > -0.1 and mean < 0.1, 'py_mean is out of range.'
    assert stddev > 0.9 and stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_05 sucessful.')  

    
def test_example_06():
    
    print('\n'+'-'*80)
    print('Starting test_example_06')

    import numpy as np
    import example_06_idlmath

    array = np.random.normal(size=1000)

    # Here we transparently call the wrapped IDL functions.
    mean = example_06_idlmath.mean(array)
    stddev = example_06_idlmath.stddev(array)
    
    assert mean > -0.1 and mean < 0.1, 'py_mean is out of range.'
    assert stddev > 0.9 and stddev < 1.1, 'py_stddev is out of range.'
    
    print('test_example_06 sucessful.')      

    
if __name__ == '__main__':
    test_example_01()  
    test_example_02()
    test_example_03()
    test_example_04()
    test_example_05()
    test_example_06()
